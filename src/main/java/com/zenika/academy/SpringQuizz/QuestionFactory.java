package com.zenika.academy.SpringQuizz;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.Random;


// La classe est finale, car un singleton n'est pas censé avoir d'héritier.
@Component
public final class QuestionFactory {

    protected int randomIndex;
    protected TmdbClient tmdbClient;
    RandomListFactory instance ;

    //constructeur
    @Autowired
    public QuestionFactory(TmdbClient tmdbClient, RandomListFactory instance ){
        this.tmdbClient = tmdbClient ;
        this.instance =instance;

    }



    // optional question existe ou pas en fonction du titre
    public Optional<Question> generateQuestionFromTitle(QuestionLevel niveau, Random random){

        Optional<MovieInfo> optionalMovieInfo = tmdbClient.getRandomPopularMovie();
        //on appelle la methode getmovie et on stock le résultat dans une var optionnel movieInfo.
        // On recupere les info d'un film à partir de son titre et on a peut être le resultat dans une variable

        /* return optionalMovieInfo.map((MovieInfo movieInfo) -> getQuestionFromMovie(movieInfo));*/
        switch (niveau){
            case HARD:
                return optionalMovieInfo.map(this::getHardQuestionFromMovie);
            case MEDIUM:
                return optionalMovieInfo.map((MovieInfo movieInfo) -> getMediumQuestionFromMovie(movieInfo, instance, random));
            case EASY:
                return optionalMovieInfo.map((MovieInfo movieInfo) -> getEasyQuestionFromMovie(movieInfo,random));
            default:
                return Optional.empty();
        }

    }

    public Question getHardQuestionFromMovie(MovieInfo movie){
        int randomInt =new Random().nextInt();
        if (randomInt > 0) {

            return new OpenQuestion("Quelle est la date de sortie du film " + movie.title + " ? ", movie.year.toString());
        }else{
                return new OpenQuestion("Quel film où jouent " + movie.cast.get(0).actorName + ", "+ movie.cast.get(1).actorName + " et " + movie.cast.get(2).actorName + " est sorti en " + movie.year.toString() ,  movie.title);

        }

    }

    public Question getMediumQuestionFromMovie(MovieInfo movie, RandomListFactory instance, Random random){

        return new MultipleChoiceQuestion("Qui n'a pas joué dans le film " + movie.title + " ? ", instance.getRandomListFactory(movie,random) ,getShuffledName(), random);
    }

    public Question getEasyQuestionFromMovie(MovieInfo movie, Random random){
/*
        randomIndex = random.nextInt(movie.cast.size());
*/
        int randomInt =new Random().nextInt();
        if (randomInt > 0) {

            return new TrueFalseQuestion("Est ce que " + movie.cast.get(getRandomIndex(movie,random)).actorName + " joue le rôle de " + movie.cast.get(getRandomIndex(movie,random)).characterName + " dans "+ movie.title + "  ?", true);
        }else{
            return new TrueFalseQuestion("Est ce que " + movie.cast.get(getRandomIndex(movie,random)+1).actorName + " joue le rôle de " + movie.cast.get(getRandomIndex(movie,random)).characterName + " dans "+ movie.title + "  ?", false);

        }

    }

    public String getShuffledName(){

        Optional<MovieInfo> optionalMovieInfo2 = tmdbClient.getRandomPopularMovie();
        return optionalMovieInfo2.get().cast.get(0).actorName;

    }

    public int getRandomIndex(MovieInfo movie, Random random){
        if(movie.cast.size() < 3){
            return randomIndex = random.nextInt(movie.cast.size());

        }else{
            return randomIndex = random.nextInt(6);
        }
    }


}

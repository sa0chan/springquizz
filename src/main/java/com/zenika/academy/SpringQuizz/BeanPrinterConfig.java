package com.zenika.academy.SpringQuizz;

import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

@Configuration
//factory de création de bean
public class BeanPrinterConfig {
    //creation d'un bean de type commandLineRunner. Spring sait qu'il faut executer ce type

            @Bean

            public File file(){
                 File fileApiKey = new File("src/main/resources/tmdb-api-key.txt");
                return fileApiKey;
            }

            @Bean
            public TmdbClient tmdbClient(){

                TmdbClient tmdbClient = null;
                try {
                    tmdbClient = new TmdbClient(file());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return tmdbClient;
            }

            @Bean
            public Scanner scan(){
                Scanner sc = new Scanner(System.in);
                return  sc ;
            }

            @Bean

         public Random random(){
                Random random = new Random();
                return random;
            }




}

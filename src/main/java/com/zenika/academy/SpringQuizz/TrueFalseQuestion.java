package com.zenika.academy.SpringQuizz;

public class TrueFalseQuestion implements Question {

    private String text;
    private boolean isTrue;
    private String answerText;



    public TrueFalseQuestion(String text, boolean isTrue) {
        this.text = text;
        this.isTrue = isTrue;
    }

    /**
     * The text of the question, preceded by the string "VRAI ou FAUX ?"
     */
    @Override
    public String getDisplayableText() {
        return "VRAI ou FAUX ? " + this.text;
    }

    @Override
    public String getDisplayableTextAnswer() {

        if(isTrue == true){
            return answerText = "Vrai";
        }else{
            return  answerText = "Faux";
        }

    }

    /**
     * Try an answer.
     *
     * If the right answer is true, accepted answers are "true", "oui", "vrai".
     * If the right answer is false, accepted answers are "false", "non", "faux".
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one INCORRECT otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        return AnswerResult.fromBoolean(
                (userAnswersYes(userAnswer) && this.isTrue) || userAnswersNo(userAnswer) && !isTrue
        );
    }

    private boolean userAnswersNo(String userAnswer) {
        String normalizedAnser = userAnswer.toLowerCase();
        return normalizedAnser.equals("false") || normalizedAnser.equals("non") || normalizedAnser.equals("faux");
    }

    private boolean userAnswersYes(String userAnswer) {
        String normalizedAnser = userAnswer.toLowerCase();
        return normalizedAnser.equals("true") || normalizedAnser.equals("oui") || normalizedAnser.equals("vrai");
    }
}

package com.zenika.academy.SpringQuizz;
import com.zenika.academy.tmdb.MovieInfo;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.*;

@Component
public class RandomListFactory {


    public RandomListFactory(){


    }

    public List<String> getRandomListFactory(MovieInfo movie, Random random) {

        Set<String> setIncorrectAnswer = new HashSet<>();
        while (setIncorrectAnswer.size() < 3){
            int randomIndex = random.nextInt(movie.cast.size());
            setIncorrectAnswer.add(movie.cast.get(randomIndex).actorName);
        }

        Iterator<String> i = setIncorrectAnswer.iterator();
        //ou faire un for each  ou encore creer une liste à partir du set new ArrayList<>(setIncorrectAnswer)

        List<String> incorrectAnswer = List.of(i.next(),i.next(),i.next());

        return incorrectAnswer;
    }
}

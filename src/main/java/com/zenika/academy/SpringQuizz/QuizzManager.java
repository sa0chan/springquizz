package com.zenika.academy.SpringQuizz;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.*;

@Component
public class QuizzManager implements CommandLineRunner{


    Scanner sc;
    Random random;
    QuestionFactory questionFactory;

    public QuizzManager(Scanner sc, Random random ,QuestionFactory questionFactory){
        this.sc = sc;
        this.random = random ;
        this.questionFactory = questionFactory;

    }



    @Override
    public void run(String... args)  {

        List<Question> questions = new ArrayList<Question>();
        questionFactory.generateQuestionFromTitle(QuestionLevel.HARD , random).ifPresent(question ->questions.add(question));
        questionFactory.generateQuestionFromTitle(QuestionLevel.MEDIUM , random ).ifPresent(question ->questions.add(question));
        questionFactory.generateQuestionFromTitle(QuestionLevel.EASY , random ).ifPresent(question ->questions.add(question));

        Player p = createPlayer(sc);

        for(Question q : questions){
            p.scorePoints(askQuestion(sc, q));
        }

        System.out.println(p.congratulations());

    }

    private static int askQuestion(Scanner sc, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = sc.nextLine();
        System.out.println("La réponse était " + q.getDisplayableTextAnswer());

        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return 2;
            case ALMOST_CORRECT:
                return 1;
            case INCORRECT:
            default:
                return 0;
        }
    }

    private static Player createPlayer(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return new Player(userName);
    }


}



package com.zenika.academy.SpringQuizz;

/**
 * Represents the status of the answer provided to a question.
 *
 * When an answer is tried for a question, the answer can be correct or incorrect but it can also
 * be "almost correct" (the meaning of this depends of the type of question).
 */
public enum AnswerResult {
    CORRECT, INCORRECT, ALMOST_CORRECT;

    /**
     * Converts `true` into CORRECT and `false` into INCORRECT
     */
    public static AnswerResult fromBoolean(boolean b) {
        return b ? CORRECT : INCORRECT;
    }
}

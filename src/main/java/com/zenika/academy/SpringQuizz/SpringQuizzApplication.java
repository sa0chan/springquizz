package com.zenika.academy.SpringQuizz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // est  egale  componentScan + config

public class SpringQuizzApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringQuizzApplication.class, args);
	}

}

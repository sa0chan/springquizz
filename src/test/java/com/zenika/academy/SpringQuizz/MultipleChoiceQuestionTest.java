package com.zenika.academy.SpringQuizz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;
import static com.zenika.academy.SpringQuizz.AnswerResult.CORRECT;
import static com.zenika.academy.SpringQuizz.AnswerResult.INCORRECT;
import static java.lang.System.lineSeparator;
import static org.junit.jupiter.api.Assertions.*;

class MultipleChoiceQuestionTest {

    private final Random seededRandom = new Random(1);

    @Test
    void displayableTextShouldIncludeSuggestions() {
        Question q = new MultipleChoiceQuestion(
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals("Qui a marché sur la lune ?" + lineSeparator() +
                "  1) Lance Armstrong" + lineSeparator() +
                "  2) Neil Armstrong" + lineSeparator() +
                "  3) Louis Armstrong" + lineSeparator(), q.getDisplayableText());
    }

    @Test
    void correctTextAnswer() {
        Question q = new MultipleChoiceQuestion(
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(CORRECT, q.tryAnswer("Neil Armstrong"));
    }

    @Test
    void correctIndexAnswer() {
        Question q = new MultipleChoiceQuestion(
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(CORRECT, q.tryAnswer("2"));
    }

    @Test
    void incorrectIndexAnswer() {
        Question q = new MultipleChoiceQuestion(
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(INCORRECT, q.tryAnswer("3"));
    }

    @Test
    void incorrectTextAnswer() {
        Question q = new MultipleChoiceQuestion(
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(INCORRECT, q.tryAnswer("Louis Armstrong"));
    }
}
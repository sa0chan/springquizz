package com.zenika.academy.SpringQuizz;
import org.junit.jupiter.api.Test;
import static com.zenika.academy.SpringQuizz.AnswerResult.CORRECT;
import static com.zenika.academy.SpringQuizz.AnswerResult.INCORRECT;
import static org.junit.jupiter.api.Assertions.*;

class TrueFalseQuestionTest {

    @Test
    void correctTrueAnswers() {
        Question q = new TrueFalseQuestion("Le mont Saint-Michel est en bretagne", true);

        assertAll(
                () -> assertEquals(CORRECT, q.tryAnswer("oui")),
                () -> assertEquals(CORRECT, q.tryAnswer("vRai")),
                () -> assertEquals(CORRECT, q.tryAnswer("True"))
        );
    }

    @Test
    void correctFalseAnswers() {
        Question q = new TrueFalseQuestion("Le mont Saint-Michel est en normandie", false);

        assertAll(
                () -> assertEquals(CORRECT, q.tryAnswer("non")),
                () -> assertEquals(CORRECT, q.tryAnswer("Faux")),
                () -> assertEquals(CORRECT, q.tryAnswer("FALSE"))
        );
    }

    @Test
    void incorrectTrueAnswers() {
        Question q = new TrueFalseQuestion("La bretagne fait le meilleur cidre", true);

        assertAll(
                () -> assertEquals(INCORRECT, q.tryAnswer("non")),
                () -> assertEquals(INCORRECT, q.tryAnswer("toto")),
                () -> assertEquals(INCORRECT, q.tryAnswer("c'est débattable"))
        );
    }

    @Test
    void incorrectFalseAnswers() {
        Question q = new TrueFalseQuestion("La normandie", false);

        assertAll(
                () -> assertEquals(INCORRECT, q.tryAnswer("yes")),
                () -> assertEquals(INCORRECT, q.tryAnswer("OK")),
                () -> assertEquals(INCORRECT, q.tryAnswer("true"))
        );
    }
}
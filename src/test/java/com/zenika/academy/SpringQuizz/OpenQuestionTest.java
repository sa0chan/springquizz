package com.zenika.academy.SpringQuizz;

import org.junit.jupiter.api.Test;

import static com.zenika.academy.SpringQuizz.AnswerResult.CORRECT;
import static com.zenika.academy.SpringQuizz.AnswerResult.INCORRECT;
import static org.junit.jupiter.api.Assertions.*;

class OpenQuestionTest {




    @Test
    void getDisplayableText() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals("Qui marche de travers ?", q.getDisplayableText());
    }

    @Test
    void tryCorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals(CORRECT, q.tryAnswer("C'est le crabe tout vert"));
    }

    @Test
    void tryIncorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals(INCORRECT, q.tryAnswer("Un homme en état d'ébriété"));
    }

    @Test
    void tryAlmostCorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals(INCORRECT, q.tryAnswer("C'est le crbe tout vart"));
    }
}
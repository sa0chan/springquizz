package com.zenika.academy.SpringQuizz;

import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class QuestionGeneratorTest {


/*    @Test
    void testGenerateHardQuestionFromTitle() {
            //Création du movieInfo
            MovieInfo movieInfo = new MovieInfo("titleTest", Year.of(1997),
                    List.of(new MovieInfo.Character("Caledon Hockley","Billy Zane")));


            TmdbClient mockTmdbClient = mock(TmdbClient.class);
            QuestionFactory questionFactory = new QuestionFactory(2,mockTmdbClient);
            when(mockTmdbClient.getRandomPopularMovie()).thenReturn(Optional.of(movieInfo));
            //get() permet de sortir de l'optional  .. pour le test prendre que la question
            assertEquals("Quelle est la date de sortie du film titleTest dans lequel joue Billy Zane" , questionFactory.generateQuestionFromTitle("titleTest",QuestionLevel.HARD).get().getDisplayableText());
            assertEquals("CORRECT" , questionFactory.generateQuestionFromTitle("titleTest",QuestionLevel.HARD).get().tryAnswer("1997").toString());

        }*/

    @Test

    void testGenerateHardQuestionFromTitleReturnEmpty()  {
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        RandomListFactory instance = new RandomListFactory() ;
        Random random = new Random(2);
        QuestionFactory questionFactory = new QuestionFactory(mockTmdbClient,instance);
        when(mockTmdbClient.getRandomPopularMovie()).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), questionFactory.generateQuestionFromTitle(QuestionLevel.HARD,random));

    }

@Test
    void testGenerateMediumQuestionFromTitle() {
        //Création du movieInfo
        MovieInfo movieInfo = new MovieInfo("titleTest", Year.of(1997),
                List.of(new MovieInfo.Character("A","a"),new MovieInfo.Character("B","b"),new MovieInfo.Character("C","c")));

        MovieInfo movieInfoOtherChar = new MovieInfo("TotoFilm" , Year.of(1887), List.of(new MovieInfo.Character("D","d"),new MovieInfo.Character("E","e"),new MovieInfo.Character("F","f")));


        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        RandomListFactory instance = new RandomListFactory() ;
        Random random = new Random(2);
        QuestionFactory questionFactory = new QuestionFactory(mockTmdbClient,instance);
        when(mockTmdbClient.getRandomPopularMovie()).thenReturn(Optional.of(movieInfo), Optional.of(movieInfoOtherChar));


        //get() permet de sortir de l'optional  .. pour le test prendre que la question
        assertEquals("CORRECT" , questionFactory.generateQuestionFromTitle(QuestionLevel.MEDIUM,random).get().tryAnswer("d").toString());

    }

    @Test

    void testGenerateMediumQuestionFromTitleReturnEmpty()  {
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        RandomListFactory instance = new RandomListFactory() ;
        Random random = new Random(2);
        QuestionFactory questionFactory = new QuestionFactory(mockTmdbClient,instance);
        when(mockTmdbClient.getRandomPopularMovie()).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), questionFactory.generateQuestionFromTitle(QuestionLevel.MEDIUM,random));

    }



}